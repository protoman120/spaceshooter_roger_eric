﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] GameObject[] lives;
    public GameObject live1;
    public GameObject live2;
    public GameObject live3;
    [SerializeField] Text score;
    [SerializeField] Text wave;

    public EnemyManagerV2 enemymanager;
    public MeteorManager meteormanager;

    private int scoreInt = 0;
    private int currentLives = 3;
    public int current_Wave;

    public int Wave_Change = 1000;
    private int Wave_Change_Multiplier = 1;

    private bool Wave_Activated;
    private bool Random_Wave_Activated;
    private int Random_Wave_Basic;
    private int Random_Wave_Medium;
    private int Random_Wave_Heavy;

    [SerializeField] GameObject MusicBackground;


    // Start is called before the first frame update
    void Awake()
    {

        //music

        if (PlayerPrefs.GetInt("music", 1) == 1)
        {
            MusicBackground.SetActive(true);
        }
        else
        {
            MusicBackground.SetActive(false);
        }

        scoreInt = 0;
        currentLives = 3;
        score.text = "000000";// scoreInt.ToString("000000");
        //score.text = scoreInt.ToString();

        current_Wave = 1;
        wave.text = "0";

        Wave_Activated = false;

        GenerateRandomWaveValues();
    }

    void Update(){

        if (scoreInt >= (Wave_Change * Wave_Change_Multiplier))
        {
            wave.text = current_Wave.ToString("0");
            current_Wave += 1;
            Wave_Change_Multiplier += 1;
            Wave_Activated = false;
            Random_Wave_Activated = true;
            Debug.Log("Deactivating Spawners");
            enemymanager.DeActivateSpawner();
            enemymanager.DisableBasicEnemies();
            enemymanager.DisablemediumEnemies();
            enemymanager.DisbleHeavyEnemies();
            enemymanager.DisableBoss1();
            meteormanager.DeActivateMeteorGenerator();
            meteormanager.MeteorSpawnRate(2);

            GenerateRandomWaveValues();

        }

        if(currentLives > 3)
        {
            currentLives = 3;
        }

        if (currentLives == 3)
        {
            live3.SetActive(true);
            live2.SetActive(true);
            live1.SetActive(true);
        }
        else if (currentLives == 2)
        {
            live3.SetActive(false);
            live2.SetActive(true);
            live1.SetActive(true);
        }
        else if (currentLives == 1)
        {
            live3.SetActive(false);
            live2.SetActive(false);
            live1.SetActive(true);
        }

        if (Wave_Activated == false)
        {
            if (current_Wave == 1)
            {
                Debug.Log("Wave 1 Initiated");
                enemymanager.ActivateSpawner();
                enemymanager.EnableBasicEnemies();
                //enemymanager.EnableHeavyEnemies();
                //enemymanager.EnablemediumEnemies();
                //enemymanager.EnableBoss1();
                //RandomBasicWave();
                //RandomMediumWave();
                //RandomHeavyWave();
                //meteormanager.ActivateMeteorGenerator();
                //meteormanager.MeteorSpawnRate(1);
            }
            else if (current_Wave == 2)
            {
                Debug.Log("Wave 2 Initiated");
                meteormanager.ActivateMeteorGenerator();
                meteormanager.MeteorSpawnRate(1);
                //enemymanager.ActivateSpawner();
                //enemymanager.EnableBoss1();
            }
            else if (current_Wave == 3)
            {
                Debug.Log("Wave 3 Initiated");
                enemymanager.ActivateSpawner();
                enemymanager.EnableBoss1();
            }
            else if (current_Wave == 4)
            {
                enemymanager.ActivateSpawner();
                enemymanager.EnablemediumEnemies();
            }
            else if (current_Wave == 5)
            {
                enemymanager.ActivateSpawner();
                enemymanager.EnablemediumEnemies();
                meteormanager.ActivateMeteorGenerator();
                meteormanager.MeteorSpawnRate(3);
            }
            else if (current_Wave == 6)
            {
                meteormanager.ActivateMeteorGenerator();
                meteormanager.MeteorSpawnRate(1);
            }
            else if (current_Wave == 7)
            {
                enemymanager.ActivateSpawner();
                enemymanager.EnablemediumEnemies();
                meteormanager.ActivateMeteorGenerator();
            }
            else if (current_Wave == 8)
            {
                enemymanager.ActivateSpawner();
                enemymanager.EnableHeavyEnemies();
            }
            else if (current_Wave == 9)
            {
                enemymanager.ActivateSpawner();
                enemymanager.EnableHeavyEnemies();
                meteormanager.ActivateMeteorGenerator();
                meteormanager.MeteorSpawnRate(4);
            }
            else if (current_Wave == 10)
            {
                enemymanager.ActivateSpawner();
                enemymanager.EnableBoss1();
            }
            else if (current_Wave > 10 && current_Wave < 20)
            {
                RandomMediumWave();
            }
            else if (current_Wave == 20)
            {
                enemymanager.ActivateSpawner();
                enemymanager.EnableBoss1();
            }
            else if (current_Wave > 20)
            {
                RandomHeavyWave();
            }
            Wave_Activated = true;
        }

    }

    void GenerateRandomWaveValues()
    {
        Random_Wave_Basic = Random.Range(1, 3);
        Random_Wave_Medium = Random.Range(1, 3);
        Random_Wave_Heavy = Random.Range(1, 3);
    }

    void RandomBasicWave()
    {
        if (Random_Wave_Activated == false)
        {
            Debug.Log("Random Basic Wave Initated");

            enemymanager.ActivateSpawner();

            if (Random_Wave_Basic == 1)
            {
                enemymanager.EnableBasicEnemies();
            }
            else if (Random_Wave_Basic == 2)
            {
                enemymanager.EnableBasicEnemies();
                meteormanager.ActivateMeteorGenerator();
            }
            else if (Random_Wave_Basic == 3)
            {
                meteormanager.ActivateMeteorGenerator();
            }
        }
    }

    void RandomMediumWave()
    {
        Debug.Log("Random Medium Wave Initated");

        enemymanager.ActivateSpawner();

        if (Random_Wave_Medium == 1)
        {
            enemymanager.EnablemediumEnemies();
        }
        else if (Random_Wave_Medium == 2)
        {
            enemymanager.EnablemediumEnemies();
            meteormanager.MeteorSpawnRate(2);
            meteormanager.ActivateMeteorGenerator();
        }
        else if (Random_Wave_Medium == 3)
        {
            meteormanager.MeteorSpawnRate(1);
            meteormanager.ActivateMeteorGenerator();
        }
    }

    void RandomHeavyWave()
    {
        Debug.Log("Random Heavy Wave Initated");

        enemymanager.ActivateSpawner();

        if (Random_Wave_Heavy == 1)
        {
            enemymanager.EnableHeavyEnemies();
        }
        else if (Random_Wave_Heavy == 2)
        {
            enemymanager.EnableHeavyEnemies();
            meteormanager.MeteorSpawnRate(1);
            meteormanager.ActivateMeteorGenerator();
        }
        else if (Random_Wave_Heavy == 3)
        {
            meteormanager.MeteorSpawnRate(0.5f);
            meteormanager.ActivateMeteorGenerator();
        }
    }

    public void AddScore(int value){
        scoreInt+=value;
        score.text = scoreInt.ToString("000000");
    }

    public void LoseLife()
    {
        currentLives--;
        if (currentLives > 0)
        {
            Debug.Log("Live Substracted");
            //lives[currentLives].SetActive(false);
        }
        else
        {
            Death();
        }
    }

    public void GainLife()
    {
        currentLives++;
        if (currentLives > 0)
        {
            Debug.Log("Live Added");
            //lives[currentLives].SetActive(true);
        }
        else if (currentLives >= 3){
            currentLives = 3;
        }else
        {
            Debug.LogError("ERROR ENABLING LIFE SPRITE");
        }
    }

    void Death()
    {

        int record = 0;

        if (PlayerPrefs.HasKey("Record"))
        {
            record = PlayerPrefs.GetInt("Record");
            if (scoreInt > record) record = scoreInt;
        }
        else
        {
            record = scoreInt;
        }
            

        PlayerPrefs.SetInt("Record", record);
        PlayerPrefs.SetInt("Score", scoreInt);

        Debug.LogError("He muerto");

        SceneManager.LoadScene("GameOver");
    }
}
