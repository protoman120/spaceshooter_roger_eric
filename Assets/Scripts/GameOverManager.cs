﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    public Text scoreText;
    public Text recordText;
    public int scoreInt = 0;
    public int recordInt = 0;

    void Start()
    {
        scoreInt = PlayerPrefs.GetInt("Score");
        recordInt = PlayerPrefs.GetInt("Record");
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = ("Score: " + scoreInt.ToString("000000"));
        recordText.text = ("Record: " + recordInt.ToString("000000"));
    }
}
