﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    public bool Player_Used;
    public GameObject ShootSprite;
    [SerializeField] AudioSource HitSound;
    
    // Update is called once per frame
    void Update () {
        transform.Translate (speed * Time.deltaTime,0, 0);
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish" || other.tag == "Meteor") {
            Destroy (gameObject);
        }
        if (Player_Used == true && other.tag == "Enemy")
        {
            StartCoroutine(DestroyBullet());
        }
    }

    IEnumerator DestroyBullet()
    {
        HitSound.Play();

        yield return new WaitForSeconds(0.1f);

        Destroy(gameObject);
    }

}
