﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject PowerUp;
    public GameObject Healing;
    public float timeLauchPowerUp;
    public float timeLauchHealing;

    public bool PowerUp_Spawn_Active;
    public bool Healing_Spawn_Active;

    private float currentTime = 0;

    void Awake(){
        if (PowerUp_Spawn_Active == true){
            StartCoroutine(SpawnPowerUp());
        }

        if (Healing_Spawn_Active == true)
        {
            StartCoroutine(Spawnhealing());
        }
    }

    IEnumerator SpawnPowerUp(){
        while(true){
            Instantiate(PowerUp,new Vector3(10,Random.Range(-5,5)),Quaternion.identity,this.transform);
            yield return new WaitForSeconds(timeLauchPowerUp);
        }
    }

    IEnumerator Spawnhealing()
    {
        while (true)
        {
            Instantiate(Healing, new Vector3(10, Random.Range(-5, 5)), Quaternion.identity, this.transform);
            yield return new WaitForSeconds(timeLauchHealing);
        }
    }
}
