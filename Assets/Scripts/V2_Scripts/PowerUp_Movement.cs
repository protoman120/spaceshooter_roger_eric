﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp_Movement : MonoBehaviour
{
    Vector2 PowerUp_speed;
    Transform PowerUp_graphics;

    public ParticleSystem ps;

    public AudioSource audioSource;

    public void Awake()
    {
        PowerUp_graphics = transform.GetChild(0);

        for(int i=0; i<PowerUp_graphics.childCount;i++){
            PowerUp_graphics.GetChild(i).gameObject.SetActive(false);
        }

        int seleccionado = Random.Range(0,PowerUp_graphics.childCount);
        PowerUp_graphics.GetChild(seleccionado).gameObject.SetActive(true);

        PowerUp_speed.x = Random.Range(-5,-2);
    }

    void Update(){
        transform.Translate(PowerUp_speed*Time.deltaTime);
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish"){
            Destroy(this.gameObject);
        } else if(other.tag == "Player") {
            StartCoroutine(PowerUp_Pickup());
        }
    }

    IEnumerator PowerUp_Pickup(){
        //Desactivo el grafico
        PowerUp_graphics.gameObject.SetActive(false);

        //Elimino el BoxCollider2D
        Destroy(GetComponent<BoxCollider2D>());

        //Lanzo la partícula
        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);
        
        //Me destruyo a mi mismo
        Destroy(this.gameObject);
    }

}
