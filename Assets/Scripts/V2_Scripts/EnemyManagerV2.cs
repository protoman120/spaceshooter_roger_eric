﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManagerV2 : MonoBehaviour
{

    public GameObject[] Basic_formations;
    public GameObject[] Medium_formations;
    public GameObject[] Heavy_formations;
    public GameObject Boss1_Sprite;
    public GameObject MeteorManager;

    private MeteorManager meteormanager;

    private bool Spawn_Basic_enemies;
    private bool Spawn_Medium_enemies;
    private bool Spawn_Heavy_enemies;
    private bool Spawn_Boss1;

    private float timeLaunchFormation_Basic;
    private float timeLaunchFormation_Medium;
    private float timeLaunchFormation_Heavy;
    private float timeLaunchFormation_Boss1;

    private bool Spawning_Enemies;
    private bool Enemy_Spawner_Activated;

    private int MeteorSpawners = 0;

    private float currentTime = 0;

    private void Awake()
    {
        timeLaunchFormation_Basic = 3;
        timeLaunchFormation_Medium = 4;
        timeLaunchFormation_Heavy = 5;
        timeLaunchFormation_Boss1 = 9999999999999999;
    }

    void Update()
    {
        if (Spawning_Enemies == true)
        {
            if (Enemy_Spawner_Activated == true)
            {
                Debug.Log("Starting Enemy Generation");
                StartCoroutine(LanzaFormacion());
                Enemy_Spawner_Activated = false;
            }
        } else if (Spawning_Enemies == false)
        {
            Debug.Log("Stopping Enemy Generation");
            StopCoroutine(LanzaFormacion());
            Enemy_Spawner_Activated = false;
            Spawn_Basic_enemies = false;
            Spawn_Medium_enemies = false;
            Spawn_Heavy_enemies = false;
            Spawn_Boss1 = false;
        }
    }

    //Spawner -------------------------------------------------
    public void ActivateSpawner()
    {
        Debug.Log("Enemy Spawner Activated");
        Enemy_Spawner_Activated = true;
        Spawning_Enemies = true;
    }

    public void DeActivateSpawner()
    {
        Debug.Log("Enemy Spawner Deactivated");
        Enemy_Spawner_Activated = false;
        Spawning_Enemies = false;
    }

    //Basic_Enemies ------------------------------------------
    public void EnableBasicEnemies()
    {
        Debug.Log("Basic Enemies Activated");
        Spawn_Basic_enemies = true;
    }

    public void DisableBasicEnemies()
    {
        Debug.Log("Basic Enemies Deactivated");
        Spawn_Basic_enemies = false;
    }

    //Medium_Enemies -----------------------------------------
    public void EnablemediumEnemies()
    {
        Debug.Log("Medium Enemies Activated");
        Spawn_Medium_enemies = true;
    }

    public void DisablemediumEnemies()
    {
        Debug.Log("Medium Enemies Deactivated");
        Spawn_Medium_enemies = false;
    }

    //Heavy_Enemies ------------------------------------------
    public void EnableHeavyEnemies()
    {
        Debug.Log("Heavy Enemies Activated");
        Spawn_Heavy_enemies = true;
    }

    public void DisbleHeavyEnemies()
    {
        Debug.Log("Heavy Enemies Deactivated");
        Spawn_Heavy_enemies = false;
    }

    //Boss1 ---------------------------------------------------

    public void EnableBoss1()
    {
        Debug.Log("Boss1 Activated");
        Spawn_Boss1 = true;
    }

    public void DisableBoss1()
    {
        Debug.Log("Boss1 Deactivated");
        Spawn_Boss1 = false;
    }

    //Time Lauch Basic ----------------------------------------
    public void BasicSpawnTimeChange(float SpawnTimeBasic)
    {
        Debug.Log("Basic Enemy Spawn Time Changed");
        timeLaunchFormation_Basic = SpawnTimeBasic;
    }

    //Time Lauch Medium ---------------------------------------
    public void MediumSpawnTimeChange(float SpawnTimeMedium)
    {
        Debug.Log("Medium Enemy Spawn Time Changed");
        timeLaunchFormation_Medium = SpawnTimeMedium;
    }

    //Time Lauch Heavy ----------------------------------------
    public void heavySpawnTimeChange(float SpawnTimeHeavy)
    {
        Debug.Log("Heavy Enemy Spawn Time Changed");
        timeLaunchFormation_Heavy = SpawnTimeHeavy;
    }

    //Time Launch Boss1 --------------------------------------

    public void Boss1SpawnTimeChange(int SpawnTimeBoss1)
    {
        Debug.Log("Boss1 Spawn Time Changed");
        timeLaunchFormation_Boss1 = SpawnTimeBoss1;
    }

    IEnumerator LanzaFormacion()
    {
        if (Spawning_Enemies == true)
        {

            if (Spawn_Basic_enemies == true)
            {

                int Basic_ActualFormation = 0;
                Debug.Log("Creating Basic Enemies");
                while (Spawn_Basic_enemies == true)
                {
                    Basic_ActualFormation = Random.Range(0, Basic_formations.Length);

                    Instantiate(Basic_formations[Basic_ActualFormation], new Vector3(10, Random.Range(-5, 5)), Quaternion.identity, this.transform);
                    yield return new WaitForSeconds(timeLaunchFormation_Basic);
                }

            }

            if (Spawn_Medium_enemies == true)
            {

                int Medium_ActualFormation = 0;
                Debug.Log("Creating Medium Enemies");
                while (Spawn_Medium_enemies == true)
                {
                    Medium_ActualFormation = Random.Range(0, Medium_formations.Length);

                    Instantiate(Medium_formations[Medium_ActualFormation], new Vector3(10, Random.Range(-5, 5)), Quaternion.identity, this.transform);
                    yield return new WaitForSeconds(timeLaunchFormation_Medium);
                }
            }

            if (Spawn_Heavy_enemies == true)
            {

                int Heavy_ActualFormation = 0;
                Debug.Log("Creating Heavy Enemies");
                while (Spawn_Heavy_enemies == true)
                {
                    Heavy_ActualFormation = Random.Range(0, Heavy_formations.Length);

                    Instantiate(Heavy_formations[Heavy_ActualFormation], new Vector3(10, Random.Range(-5, 5)), Quaternion.identity, this.transform);
                    yield return new WaitForSeconds(timeLaunchFormation_Heavy);
                }
            }

            if (Spawn_Boss1 == true)
            {
                Boss1_Sprite.SetActive(true);
                Instantiate(Boss1_Sprite, new Vector3(10, Random.Range(-5, 5)), Quaternion.identity, this.transform);
                yield return new WaitForSeconds(timeLaunchFormation_Boss1);
            }
        }
    }
    /*    
    void Meteorite_Controller()
    {
        if (MeteorSpawner_Active == true){
            
            if (MeteorSpawners < 1){
                MeteorManager.SetActive(true);
                MeteorSpawners++;
            }

        }else if (MeteorSpawner_Active == false){

            MeteorManager.SetActive(false);
            if (MeteorSpawners > 0){
                MeteorSpawners--;
            }
        }
    }
    */
}
