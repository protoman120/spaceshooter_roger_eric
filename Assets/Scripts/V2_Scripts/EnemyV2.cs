﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyV2 : MonoBehaviour
{

    public bool Basic_Enemy_Used;
    public bool Medium_Enemy_Used;
    public bool Heavy_Enemy_Used;
    public bool Boss1_Used;

    [SerializeField] GameObject[] BasicEnemy_sprites;
    [SerializeField] GameObject[] MediumEnemy_sprites;
    [SerializeField] GameObject[] HeavyEnemy_sprites;

    public GameObject Boss1_Idle;
    public GameObject Boss1_StartShooting;
    public GameObject Boss1_Shooting;
    public GameObject Boss1_FinishShooting;

    private bool Boss1_Charging;
    private bool Boss1_ShootingState;
    private bool Boss1_FinishShootingState;
    private bool Boss1_StartShootingState;
    public int Boss1_FinishShootingTime;
    private int Boss1_StartShootTimer;
    public int Boss1_StartShootingTime;
    private int Boss1_ShootTimer;
    private int Boss1_FinishShootTimer;
    public int Boss1_ShootTime;
    private bool Boss1_HasEntered;
    private bool Boss1_Movement = false;
    public int Boss1_MovementTime = 1;
    private int Boss1_MovementTimer = 0;
    private int Boss1_Direction;
    public float Boss1_SpeedX;
    public float Boss1_SpeedY;
    public float Boss1_MaxPosX = 20;
    private bool Boss1_IsDead = false;


    public bool PowerUp_Can_Drop;
    public bool Healing_Can_Drop;

    public GameObject PowerUp_sprite;
    public GameObject Healing_Sprite;

    private int PowerUp_DropChance;
    private int Healing_DropChance;

    private int PowerUp_DropChance_result;
    private int Healing_DropChance_result;

    private int Sprite_elegida;

    [SerializeField] BoxCollider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;

    private float timeCounter;
    private float timeToShoot;

    private float timeShooting;
    private float speedx;

    private bool isShooting;

    private ScoreManager sm;

    public int puntuacion = 100;
    public int score_medium = 150;
    public int score_heavy = 200;
    public int score_Boss1 = 1000;

    public int life_medium = 1;
    public int life_Heavy = 3;
    public int life_Boss1 = 50;

    [SerializeField] GameObject bullet;

    private bool Enemy_CanShoot = true;

    public float limitX = 8.4f;
    public float limitY = 4.4f;
    public float limitYNegative = -4.4f;

    private void Awake()
    {

        if (Basic_Enemy_Used == true){
            
            Sprite_elegida = Random.Range(0, BasicEnemy_sprites.Length);

            PowerUp_DropChance = 10;
            Healing_DropChance = 5;

            for (int kk = 0; kk < BasicEnemy_sprites.Length; kk++)
            {
                BasicEnemy_sprites[kk].SetActive(false);
            }

            BasicEnemy_sprites[Sprite_elegida].SetActive(true);

            sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();
        }

        if (Medium_Enemy_Used == true){
            
            Sprite_elegida = Random.Range(0, MediumEnemy_sprites.Length);

            PowerUp_DropChance = 20;
            Healing_DropChance = 8;

            for (int kk = 0; kk < MediumEnemy_sprites.Length; kk++)
            {
                MediumEnemy_sprites[kk].SetActive(false);
            }

            MediumEnemy_sprites[Sprite_elegida].SetActive(true);

            sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();

        }

        if (Heavy_Enemy_Used == true){
            
            Sprite_elegida = Random.Range(0, HeavyEnemy_sprites.Length);

            PowerUp_DropChance = 30;
            Healing_DropChance = 10;

            for (int kk = 0; kk < HeavyEnemy_sprites.Length; kk++)
            {
                HeavyEnemy_sprites[kk].SetActive(false);
            }

            HeavyEnemy_sprites[Sprite_elegida].SetActive(true);

            sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();
        }

        if (Boss1_Used == true)
        {
            Boss1_Idle.SetActive(true);
            PowerUp_DropChance = 80;
            Healing_DropChance = 50;
            Boss1_Movement = false;
        }

        Healing_DropChance_result = Random.Range(0, 100);
        PowerUp_DropChance_result = Random.Range(0, 100);
        sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();

        Inicitialization();
    }

    void Update()
    {
        if (Boss1_Used == true)
        {
            Boss1_Behaviour();
        }
        else EnemyBehaviour();
    }

    protected virtual void Inicitialization()
    {
        timeCounter = 0.0f;
        timeToShoot = 1.0f;
        timeShooting = 1.0f;
        speedx = 3.0f;
        isShooting = false;
    }

    protected virtual void EnemyBehaviour()
    { 
        timeCounter += Time.deltaTime;

        if (timeCounter > timeToShoot)
        {
            if (Enemy_CanShoot == true)
            {
                if (!isShooting)
                {
                    isShooting = true;
                    Instantiate(bullet, this.transform.position, Quaternion.Euler(0, 0, 180), null);
                }
            }
            if (timeCounter > (timeToShoot + timeShooting))
            {
                timeCounter = 0.0f;
                isShooting = false;
            }
        }
        else
        {
            transform.Translate(-speedx * Time.deltaTime, 0, 0);
        }

    }

    protected virtual void Boss1_Behaviour()
    {
        timeCounter += Time.deltaTime;
        if (Boss1_Movement == false)
        {
            //Boss1_Idle.SetActive(true);
            if (Boss1_HasEntered == false)
            {
                for (int i = 0; i <= 100; i++)
                {
                    transform.Translate(-Boss1_SpeedX * Time.deltaTime, 0, 0);
                }
                Boss1_HasEntered = true;

            }else if (Boss1_HasEntered == true)
            {
                Debug.Log("Changing Boss 1 moment to Y");
                Boss1_Direction = 1;
                Boss1_MovementTimer = 0;
                Boss1_Movement = true;
                Boss1_ShootingState = false;
            }
            /*
            if (Boss1_MovementTime >= Boss1_MovementTimer)
            {
                Debug.Log("Moving Boss 1 in X");
                transform.Translate(-Boss1_SpeedX * Time.deltaTime, 0, 0);
                Boss1_MovementTimer++;
            }
            
            if (Boss1_MovementTime <= Boss1_MovementTimer)
            {
                Debug.Log("Changing Boss 1 moment to Y");
                Boss1_Direction = 1;
                Boss1_MovementTimer = 0;
                Boss1_Movement = true;
                Boss1_ShootingState = false;
            }
            */
        }

        if (Boss1_Movement == true)
        {
            //Boss1_Idle.SetActive(false);
            /*
            Debug.Log("Boss 1: Shooting enabled");
            if (Boss1_StartShootTimer > timeCounter)
            {
                Debug.Log("Boss 1: Started Shooting");
                Boss1_ShootingState = true;
                Instantiate(bullet, this.transform.position, Quaternion.Euler(0, 0, 180), null);
            }
            if (timeCounter > Boss1_ShootTime)
            {
                Debug.Log("Boss 1: Stopped Shooting");
                timeCounter = 0.0f;
                Boss1_ShootTime = 0;
                Boss1_ShootingState = false;
            }
            */


            if (Boss1_Direction == 1)
            {
                Debug.Log("Moving Boss 1 in Y UP");
                transform.Translate(0, Boss1_SpeedY * Time.deltaTime, 0);
                Boss1_ShootTimer++;
                Boss1_StartShootTimer++;

                if (transform.position.y > limitY)
                {
                    Boss1_Direction = 2;
                    Boss1_ShootingLogic();
                }
                if (Boss1_ShootTimer > Boss1_ShootTime)
                {
                    if (Boss1_ShootingState == true)
                    {
                        Instantiate(bullet, this.transform.position, Quaternion.Euler(0, 0, 180), null);
                    }
                }
                else if (Boss1_ShootTimer > 31)
                {
                    Boss1_ShootTimer = 0;
                }


            }
            else if (Boss1_Direction == 2)
            {
                Debug.Log("Moving Boss 1 in Y DOWN");
                transform.Translate(0, -Boss1_SpeedY * Time.deltaTime, 0);
                Boss1_ShootTimer++;
                if (transform.position.y < -limitY)
                {
                    Boss1_Direction = 1;
                    Boss1_ShootingLogic();
                }
                if (Boss1_ShootTimer > Boss1_ShootTime)
                {
                    if (Boss1_ShootingState == true)
                    {
                        Instantiate(bullet, this.transform.position, Quaternion.Euler(0, 0, 180), null);
                    }
                }else if (Boss1_ShootTimer > 31)
                {
                    Boss1_ShootTimer = 0;
                }
            }
        }
    }

    void Boss1_ShootingLogic()
    {
        if (Boss1_IsDead == false)
        {
            if (Boss1_StartShootingState == false && Boss1_ShootingState == false)
            {
                Boss1_StartShootingState = true;
                //Boss1_StartShooting.SetActive(true);
            }
            else if (Boss1_StartShootingState == true && Boss1_ShootingState == false)
            {
                Boss1_StartShootingState = false;
                //Boss1_StartShooting.SetActive(false);
                Boss1_ShootingState = true;
                //Boss1_Shooting.SetActive(true);
            }
            else if (Boss1_ShootingState == true)
            {
                Boss1_ShootingState = false;
                //Boss1_Shooting.SetActive(false);
            }
            else if (Boss1_FinishShootingState == false && Boss1_StartShootingState == false)
            {
                Boss1_FinishShootingState = true;
                //Boss1_FinishShooting.SetActive(true);
            }
            else if (Boss1_FinishShootingState == true && Boss1_StartShootingState == false)
            {
                Boss1_FinishShootingState = false;
                //Boss1_FinishShooting.SetActive(false);
                Boss1_StartShootingState = true;
                //Boss1_StartShooting.SetActive(true);
            }
        }
    }


    /*
        if (Boss1_HasEntered == true)
        {
            
            if (Boss1_ShootingState == false)
            {
                if (Boss1_Movement == true && transform.position.y <= limitY)
                {
                    transform.Translate(0, Boss1_SpeedY * Time.deltaTime, 0);
                }
                else if (Boss1_Movement == false && transform.position.y <= limitYNegative)
                {
                    transform.Translate(0, -Boss1_SpeedY * Time.deltaTime, 0);
                }

                if (transform.position.y <= limitY)
                {
                    Boss1_Movement = true;
                }else if (transform.position.y <= (limitY * -1))
                {
                    Boss1_Movement = false;
                }

                if (Boss1_ShootTime < Boss1_ShootTimer)
                {
                    Boss1_ShootTimer++;
                }
                else if (Boss1_ShootTime > Boss1_ShootTimer)
                {
                    Boss1_ShootTimer = 0;
                    Boss1_ShootingState = true;
                    Boss1_Movement = false;
                }
            }
            else if (Boss1_ShootingState == true)
            {
                Boss1_Idle.SetActive(false);
                if (Boss1_StartShootingTime < Boss1_StartShootTimer)
                {
                    Debug.Log("Boss1 Start Shooting");
                    Boss1_StartShooting.SetActive(true);
                    Boss1_StartShootingState = true;
                }
                else if (Boss1_ShootTime > Boss1_ShootTimer)
                {
                    Boss1_StartShootingState = false;
                }

                if (Boss1_StartShootingState == false)
                {
                    if (Boss1_ShootTime < Boss1_ShootTimer)
                    {
                        Debug.Log("Boss1 Shooting");
                        Boss1_StartShooting.SetActive(false);
                        Boss1_Shooting.SetActive(true);
                        Instantiate(bullet, this.transform.position, Quaternion.Euler(0, 0, 180), null);
                        Boss1_ShootTimer++;
                    }
                    else if (Boss1_ShootTime > Boss1_ShootTimer)
                    {
                        if (Boss1_FinishShootingTime < Boss1_FinishShootTimer)
                        {
                            Debug.Log("Boss1 Stop Shooting");
                            Boss1_FinishShooting.SetActive(true);
                            Boss1_Shooting.SetActive(false);
                        }
                        Boss1_ShootingState = false;
                        Boss1_Movement = true;
                        Boss1_StartShootingState = false;
                        Boss1_ShootTimer = 0;
                        Boss1_FinishShootTimer = 0;
                    }
                }
            }
        }
        else if (Boss1_HasEntered == false)
        {
            Debug.Log("Boss1 entering");
            transform.Translate(-Boss1_SpeedX * Time.deltaTime, 0, 0);
            Boss1_HasEntered = true;
            Boss1_Movement = true;
            Boss1_ShootingState = false;
        }
    */

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet")
        {
            if (Basic_Enemy_Used == true)
            {
                if (Healing_DropChance_result < Healing_DropChance)
                {
                    Instantiate(Healing_Sprite, this.transform.position, Quaternion.identity, null);
                }
                if (PowerUp_DropChance_result < PowerUp_DropChance)
                {
                    Instantiate(PowerUp_sprite, this.transform.position, Quaternion.identity, null);
                }
                StartCoroutine(DestroyShip());
            }else if (Medium_Enemy_Used == true)
            {
                if (life_medium > 0)
                {
                    life_medium--;
                }
                else
                {
                    if (Healing_DropChance_result < Healing_DropChance)
                    {
                        Instantiate(Healing_Sprite, this.transform.position, Quaternion.identity, null);
                    }
                    if (PowerUp_DropChance_result < PowerUp_DropChance)
                    {
                        Instantiate(PowerUp_sprite, this.transform.position, Quaternion.identity, null);
                    }
                    StartCoroutine(DestroyShip());
                }
            }else if (Heavy_Enemy_Used == true)
            {
                if(life_Heavy > 0)
                {
                    life_Heavy--;
                }
                else
                {
                    if (Healing_DropChance_result < Healing_DropChance)
                    {
                        Instantiate(Healing_Sprite, this.transform.position, Quaternion.identity, null);
                    }
                    if (PowerUp_DropChance_result < PowerUp_DropChance)
                    {
                        Instantiate(PowerUp_sprite, this.transform.position, Quaternion.identity, null);
                    }
                    StartCoroutine(DestroyShip());
                }
            }else if (Boss1_Used == true)
            {
                if (life_Boss1 > 0)
                {
                    life_Boss1--;
                }
                else
                {
                    if (Healing_DropChance_result < Healing_DropChance)
                    {
                        Instantiate(Healing_Sprite, this.transform.position, Quaternion.identity, null);
                    }
                    if (PowerUp_DropChance_result < PowerUp_DropChance)
                    {
                        Instantiate(PowerUp_sprite, this.transform.position, Quaternion.identity, null);
                    }
                    StartCoroutine(Destroy_Boss1());
                }
            }
        }else if (other.tag == "Finish")
        {
            if (Boss1_Used)
            {
                return;
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
    }


    IEnumerator DestroyShip()
    {
        Enemy_CanShoot = false;

        //Desactivo el grafico
        if (Basic_Enemy_Used == true){
            BasicEnemy_sprites[Sprite_elegida].SetActive(false);
            sm.AddScore(puntuacion);
        }

        if (Medium_Enemy_Used == true){
            MediumEnemy_sprites[Sprite_elegida].SetActive(false);
            sm.AddScore(score_medium);
        }

        if (Heavy_Enemy_Used == true){
           HeavyEnemy_sprites[Sprite_elegida].SetActive(false);
            sm.AddScore(score_heavy);
        }

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo la partícula
        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        Destroy(this.gameObject);
    }

    IEnumerator Destroy_Boss1()
    {
        //Sumar puntos
        sm.AddScore(score_Boss1);

        Boss1_IsDead = true;

        //Desactivo el grafico
        if (Basic_Enemy_Used == true)
        {
            BasicEnemy_sprites[Sprite_elegida].SetActive(false);
        }

        if (Medium_Enemy_Used == true)
        {
            MediumEnemy_sprites[Sprite_elegida].SetActive(false);
        }

        if (Heavy_Enemy_Used == true)
        {
            HeavyEnemy_sprites[Sprite_elegida].SetActive(false);
        }

        if (Boss1_Used == true)
        {
            Boss1_Idle.SetActive(false);
            Boss1_Shooting.SetActive(false);
            Boss1_StartShooting.SetActive(false);
            Boss1_FinishShooting.SetActive(false);
        }

        Boss1_ShootingState = false;
        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo la partícula
        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        Destroy(this.gameObject);
    }
}
