﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviourV2 : MonoBehaviour
{

    public float speed;
    private Vector2 axis;
    public Vector2 limits;

    private float shootTime=0;

    public Weapon Stage1_weapon;
    public Weapon Stage2_weapon;
    public Weapon Stage3_weapon;

    public Propeller prop;
    
    public int Ship_Stage = 1;
    [SerializeField] GameObject Stage_1_Graphics;
    [SerializeField] GameObject Stage_2_Graphics;
    [SerializeField] GameObject Stage_3_Graphics;

    [SerializeField] Collider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;
    [SerializeField] ScoreManager scoreManager;

    //private int Speed_Module_Value = 100;
    //private bool Speed_Module_Active = false;
    //public int Speed_module_Discharge = 1;

    public int lives = 3;
    private bool iamDead = false;
    
    // Update is called once per frame
    void Update () {
        if(iamDead){
            return;
        }

        if (lives > 3)
        {
            lives = 3;
        }

        shootTime += Time.deltaTime;

        transform.Translate (axis * speed * Time.deltaTime);

        if (transform.position.x > limits.x) {
            transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
        }else if (transform.position.x < -limits.x) {
            transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
        }

        if (transform.position.y > limits.y) {
            transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
        }else if (transform.position.y < -limits.y) {
            transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
        }

        if(axis.x>0){
            prop.BlueFire();
        }else if(axis.x<0){
            prop.RedFire();
        }else{
            prop.Stop();
        }

        if (Ship_Stage == 1){
            Stage_1_Graphics.SetActive(true);
            Stage_2_Graphics.SetActive(false);
            Stage_3_Graphics.SetActive(false);
        }else if (Ship_Stage == 2){
            Stage_1_Graphics.SetActive(false);
            Stage_2_Graphics.SetActive(true);
            Stage_3_Graphics.SetActive(false);
        }else if (Ship_Stage == 3){
            Stage_1_Graphics.SetActive(false);
            Stage_2_Graphics.SetActive(false);
            Stage_3_Graphics.SetActive(true);
        }
/*
        if (Speed_Module_Value <= 0)
        {
            Speed_Module_Value = 0;
            Speed_Module_Active = false;
        }
        else if (Speed_Module_Value >= 100)
        {
            Speed_Module_Value = 100;
        }

        if (Speed_Module_Value < 100 && Speed_Module_Active == false)
        {
            Speed_Module_Value++;
        }

        if (Speed_Module_Active == true)
        {
            Speed_Module_Value -= Speed_module_Discharge;
            scoreManager.SpeedModule(Speed_module_Discharge);
        }
*/
    }
    public void ActualizaDatosInput(Vector2 currentAxis){
        axis = currentAxis;
    }

    public void SetAxis(float x, float y){
        axis = new Vector2(x,y);
    }

    public void SetAxis(Vector2 currentAxis){
        axis = currentAxis;
    }

    public void Shoot(){
        if (Ship_Stage == 1){
            if(shootTime>Stage1_weapon.GetCadencia()){
                shootTime = 0f;
                Stage1_weapon.Shoot();
            }
        }else if (Ship_Stage == 2){
            if(shootTime>Stage2_weapon.GetCadencia()){
                shootTime = 0f;
                Stage2_weapon.Shoot();
            }
        }else if (Ship_Stage == 3){
            if(shootTime>Stage3_weapon.GetCadencia()){
                shootTime = 0f;
                Stage3_weapon.Shoot();
            }
        }
    }
 /*   public void SpeedModule()
    {
        if (Speed_Module_Active == false)
        {
            Speed_Module_Active = true;
        }else if (Speed_Module_Active == true)
        {
            Speed_Module_Active = false;
        }
    }
*/
    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    public void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Meteor" || other.tag == "EnemyBullet" || other.tag == "Enemy") {
            StartCoroutine(DestroyShip());
        }else if (other.tag == "PowerUp"){
            UpgradeShip();
        }else if (other.tag == "Healing")
        {
            GainLife();
            scoreManager.GainLife();
        }
    }

    IEnumerator DestroyShip(){
        //Indico que estoy muerto
        iamDead=true;

        //Me quito una vida
        lives--;
        
        //Desactivo el grafico
        if (Ship_Stage == 1){
            Stage_1_Graphics.SetActive(false);
        }else if (Ship_Stage == 2){
            Stage_2_Graphics.SetActive(false);
        }else if (Ship_Stage == 3){
            Stage_3_Graphics.SetActive(false);
        }

        Ship_Stage = 1;
        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo la partícula
        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Desactivo el propeller
        prop.gameObject.SetActive(false);

        //Indicamos al score que hemos perdido una vida
        scoreManager.LoseLife();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);
        
        //Miro si tengo mas vidas
        if(lives>0){
            StartCoroutine(inMortal());
        }
    }

    IEnumerator inMortal(){
        //Vuelvo a activar el jugador
        iamDead = false;
        Stage_1_Graphics.SetActive(true);
        //Activo el propeller
        prop.gameObject.SetActive(true);

        for(int i=0;i<15;i++){
            Stage_1_Graphics.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            Stage_1_Graphics.SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }

        //Activo el collider
        collider.enabled = true;
    }

    void UpgradeShip(){
        if (Ship_Stage == 1){ 
            Ship_Stage = 2;
        }else if (Ship_Stage == 2){
            Ship_Stage = 3;
        }else if (Ship_Stage < 3){
            Ship_Stage = 3;
        }
    }
    void GainLife()
    {
        if (lives == 1)
        {
            lives = 2;
        }else if (lives == 2)
        {
            lives = 3;
        }else if (lives < 3)
        {
            lives = 3;
        }
    }
}
