﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public bool animacion = false;
    public Animator titleAnimation;
    public Animator startButtonAnimation;
    public Animator optionsAnimation;
    public Animator exitAnimation;
    public Animator creditsAnimation;

    //private bool soundgame;

    [SerializeField] GameObject MusicBackground;

    /*
    [SerializeField] GameObject Title;
    [SerializeField] GameObject StartButton;
    [SerializeField] GameObject OptionsButton;
    [SerializeField] GameObject ExitButton;
    [SerializeField] GameObject CreditsButton;
    */
    /*
    void Start()
    {
        titleAnimation = GetComponent<Animator>();
        startButtonAnimation = GetComponent<Animator>();
        optionsAnimation = GetComponent<Animator>();
        exitAnimation = GetComponent<Animator>();
        creditsAnimation = GetComponent<Animator>();
    }*/




    void Awake()
    {
        if (PlayerPrefs.GetInt("music", 1) == 1)
        {
            MusicBackground.SetActive(true);
        }
        else
        {
            MusicBackground.SetActive(false);
        }


        if (!animacion)
        {
            titleAnimation.enabled = false;
            startButtonAnimation.enabled = false;
            optionsAnimation.enabled = false;
            exitAnimation.enabled = false;
            creditsAnimation.enabled = false;

            
        }
    }


    public void PulsaPlay(){
        Debug.LogError("He pulsado Play");

        SceneManager.LoadScene("Game");
    }

    public void PulsaOptions(){
        Debug.LogError("He pulsado Options");

        //StartCoroutine(DisableAnimation());
        PlayerPrefs.SetInt("animacion", 0);

        SceneManager.LoadScene("Options");
    }

    public void PulsaCredits()
    {
        Debug.LogError("He pulsado Credits");

       // StartCoroutine(DisableAnimation());

        SceneManager.LoadScene("Credits");
    }

    public void PulsaExit(){
        Application.Quit();
    }

    /*
    IEnumerator DisableAnimation()
    {
        Debug.LogError("Desactivar animacion");

        titleAnimation.enabled = false;


        //Animator.setActive(false);

        //Destroy(GetComponent<Animator>());

        //Animator.SetActive(false);

    }*/
}
