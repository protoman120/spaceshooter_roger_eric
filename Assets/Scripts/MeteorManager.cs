﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorManager : MonoBehaviour
{
    public GameObject meteorPrefab;
    private float timeLauchMeteor;
    private float timeLauchMeteorChange;
    private bool MeteorGeneratorActive;
    private bool MeteorGeneratorActivated;

    private float currentTime = 0;

    private void Awake()
    {
        MeteorGeneratorActivated = false;
        MeteorGeneratorActive = false;
        timeLauchMeteor = 2;
    }

    void Update()
    {
        if (MeteorGeneratorActive == true)
        {
            if (MeteorGeneratorActivated == true)
            {
                Debug.Log("Starting Meteor Generation");
                StartCoroutine(LanzaMeteoritos());
                MeteorGeneratorActivated = false;
            }

        }else if (MeteorGeneratorActive == false)
        {
            Debug.Log("Stopping Meteor Generation");
            StopCoroutine(LanzaMeteoritos());
            MeteorGeneratorActivated = false;
        }
    }

    // Update is called once per frame
    /*
    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime>timeLauchMeteor){
            currentTime = 0;
            Instantiate(meteorPrefab,new Vector3(10,Random.Range(-5,5)),Quaternion.identity,this.transform);
        }
    }
    */

    public void ActivateMeteorGenerator()
    {
        Debug.Log("MeteorManager: Meteors Activated");
        MeteorGeneratorActive = true;
        MeteorGeneratorActivated = true;
    }

    public void DeActivateMeteorGenerator()
    {
        Debug.Log("MeteorManager: Meteors Deactivated");
        MeteorGeneratorActive = false;
        MeteorGeneratorActivated = false;
    }

    public void MeteorSpawnRate(float Rate)
    {
        Debug.Log("MeteorManager: Spawn value received");
        timeLauchMeteor = Rate;
    }

    IEnumerator LanzaMeteoritos(){
        while (MeteorGeneratorActive == true){
            Instantiate(meteorPrefab,new Vector3(10,Random.Range(-5,5)),Quaternion.identity,this.transform);
            yield return new WaitForSeconds(timeLauchMeteor);
        }
    }

}